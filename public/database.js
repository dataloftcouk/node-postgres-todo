require('dotenv').config();
const pg = require('pg');
const log = require('../logger');
let pool = new pg.Pool(),
    express = require('express'),
    app = express(),
    server = require('http').Server(app),
    logger = require('log4js').getLogger(),
    port = 8124,
    lats,
    adres,
    arrcoord = [[]];


const connectionString = process.env.DATABASE_URL;
const client = new pg.Client({connectionString});
client.connect();
server.listen(port, '127.0.0.1', function () {
    let addr = server.address();
    logger.level = 'debug';
    logger.debug('listening on ' + addr.address + ':' + addr.port);
});

console.log('Сервер успешно запущен, порт ' + port);

app.use(express.static(__dirname + '/public'));

let bodyParser = require('body-parser');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}));


/*const newst = {
    text: 'INSERT INTO student(id, name, rollnumber) VALUES($1, $2, $3)',
    values: [11, 'Милана', 10011],
};

// callback
client.query(newst, (err, res) => {
    if (err) {
        console.log(1, err.stack)
    } else {
        console.log('передача', res.rows)
    }
});*/
app.use(express.static('public'));
let i = 3;
while (i < 6) {
    addRoute_sendAdres(i);
    console.log('i=',i);
    i++;
}

function addRoute_sendAdres(k) {
    console.log(k);
    client.query('SELECT * FROM adressmap where id = $1', [k], (err, result) => {
        if (err) {
            console.log(err);
        }
        else {
            lats = [result.rows[0].latitude, result.rows[0].longitude];
            console.log('координаты для передачи:', lats);


            app.get('/testajax', function (req, res) {
                //lats = [51.7489400161863, -1.25743980506112];
                res.send(lats);
                console.log(3, lats);
            });

            app.post('/testa', function (data) {
                adres = data.body;
                console.log(adres);
                console.log(lats[0]);
                sendAdressDB(adres, lats[0]);
            });
        }
    });

    function sendAdressDB(adres, lat) {
        client.query('UPDATE adressmap SET adress = $1 WHERE latitude = $2', [adres, lat], (err, res) => {
            if (err) {
                console.log(1, err.stack)
            } else {
                console.log('adres:', adres);
            }
        });
    }
}