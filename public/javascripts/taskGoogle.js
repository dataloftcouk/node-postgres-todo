let lats,
    lons,
    i = 0;

//while (i < 1) {
function initMap() {

    console.log('initMap');
    let map = new google.maps.Map(document.getElementById('map'), {
            zoom: 8,
            center: {lat: 40.731, lng: -73.997}
        }),
        geocoder = new google.maps.Geocoder,
        infowindow = new google.maps.InfoWindow;

    console.log('start Search');

    $.ajax({
        url: '/testajax',
        success: function (data) {
            str = '';
            if (data instanceof Array) str = JSON.stringify(data);
            // alert('Load was performed.');
            lats = data[0];
            lons = data[1];
            console.log('Загружены данные' + data,);
            geocodeLatLng(geocoder, map, infowindow, lats, lons);

        }
    });
    /*   }

       i++;*/
}

function geocodeLatLng(geocoder, map, infowindow, lats, lons) {
    console.log(lats, lons);
    let latlng = {lat: parseFloat(lats), lng: parseFloat(lons)};
    geocoder.geocode({'location': latlng}, function (results, status) {
        if (status === 'OK') {
            if (results[0]) {
                map.setZoom(11);
                let marker = new google.maps.Marker({
                    position: latlng,
                    map: map
                });
                let adress = results[0].formatted_address;
                infowindow.setContent(adress);

                $.ajax({
                    type: "POST",
                    url: "/testa",
                    data: adress,
                    success: function (msg) {
                        alert("Прибыли данные: " + msg);
                    }
                });
                infowindow.open(map, marker);
                console.log('Adress: ', adress);
            } else {
                window.alert('No results found');
            }
        } else {
            window.alert('Geocoder failed due to: ' + status);
        }
    });
}